# markdown-it-internal-link

[![npm version](https://img.shields.io/npm/v/markdown-it-internal-link)](https://www.npmjs.com/package/markdown-it-internal-link) [![pipeline status](https://gitlab.com/charlyisidore/markdown-it-internal-link/badges/main/pipeline.svg)](https://gitlab.com/charlyisidore/markdown-it-internal-link/-/commits/main) [![coverage report](https://gitlab.com/charlyisidore/markdown-it-internal-link/badges/main/coverage.svg)](https://gitlab.com/charlyisidore/markdown-it-internal-link/-/commits/main)

> Internal links plugin for [markdown-it](https://github.com/markdown-it/markdown-it) markdown parser.

Use this plugin to parse and render `[[internal links]]` (also called *wikilinks* or *roamlinks*).

## Install

Node:

```
npm install --save markdown-it-internal-link
```

## Usage

Node:

```js
const md = require("markdown-it")();
const markdownItInternalLink = require("markdown-it-internal-link");

md.use(markdownItInternalLink, options);
```

Browser:

```html
<script src="https://unpkg.com/markdown-it-internal-link@1/dist/index.umd.js"></script>
```

```js
const md = window.markdownit();

md.use(window.markdownItInternalLink, options);
```

By default, all `[[bracketed]]` links are converted to `<a href="bracketed">bracketed</a>`. To override this behavior, specify the `options` parameter as either an *object* or a *function*.

## Options

If `options` is an object, the plugin uses its properties to make the HTML attributes of the `<a>` element, except `options.text` used for the inner text. All the properties can be given as strings, or functions (`(content: string, env: any) => string`) called for each link. The `content` argument is a string corresponding to the text inside double-brackets. The `env` argument is the environment object passed to `markdown-it` before rendering.

Example:

```js
md.use(markdownItInternalLink, {
    // add a class attribute
    class: "my-link",

    // add a title attribute
    title: "click me",

    // override the inner text
    text: (content) => content.toUpperCase()
});

md.render("[[foo]]");
// => `<a class="my-link" href="foo" title="click me">FOO</a>`
```

If `options` is a function, it must have the signature `(content: string, env: any) => (object | string)`. This function is called for each link. It can return an object with a structure similar to the previous example. For example, how to render *wikilinks* with optional label (piped links):

```js
// Render wikilinks with optional label
md.use(markdownItInternalLink, (content) => {
    // search for the pipe character
    const pos = content.indexOf("|");

    return {
        // add a class attribute
        class: "wikilink",

        // override the href attribute
        href: pos === -1 ? content : content.substring(0, pos),

        // override the inner text
        text: pos === -1 ? content : content.substring(pos + 1)
    };
});

md.render("[[foo]]"); // => `<a class="wikilink" href="foo">foo</a>`

md.render("[[foo|bar]]"); // => `<a class="wikilink" href="foo">bar</a>`
```

To fully control the rendering, the function can return a string instead.
The following example converts internal links to `<button>` elements:

```js
// Render buttons
md.use(markdownItInternalLink, (content) => `<button>${content}</button>`);

md.render("[[foo]]"); // => `<button>foo</button>`
```

One can also use the `env` parameter to specify a per-render context.
The following example collects all the internal links of the document:

```js
md.use(markdownItInternalLink, (content, env) =>
    env.internalLinks.push(content)
);

const env = { internalLinks: [] };

md.render("[[foo]] and [[bar]]", env);
// => `<a href="foo">foo</a> and <a href="bar">bar</a>`

console.log(env.internalLinks); // => ["foo", "bar"]
```

## Contributing

Contributions are welcome. When contributing to this repository, please first discuss the change you wish to make via [issue](https://gitlab.com/charlyisidore/markdown-it-internal-link/issues), email, or any other method with the owners of this repository before making a change.

## License

This software is licensed under the MIT license.
Please see the `LICENSE` file for further information.