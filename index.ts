import type MarkdownIt from "markdown-it";
import type Renderer from "markdown-it/lib/renderer";
import type Token from "markdown-it/lib/token";
import type StateInline from "markdown-it/lib/rules_inline/state_inline";

type OptionsAsAttrs = Record<
    string,
    string | ((content: string, env: unknown) => string)
>;

type Options =
    | OptionsAsAttrs
    | ((content: string, env: unknown) => string | Record<string, string>);

export default function (md: MarkdownIt, options?: Options): void {
    const openBracketChar = "[".charCodeAt(0);
    const closeBracketChar = "]".charCodeAt(0);
    const newlineChar = "\n".charCodeAt(0);

    function tokenize(state: StateInline, silent: boolean) {
        if (silent) return false;

        const start = state.pos;
        const max = state.posMax;

        // at least 5 characters long (including brackets)
        if (start + 5 >= max) return false;

        // must start with a first '['
        if (state.src.charCodeAt(start) !== openBracketChar) return false;
        // must start with a second '['
        if (state.src.charCodeAt(start + 1) !== openBracketChar) return false;

        let pos;
        let foundEnd = false;

        // read content until "]]"
        for (pos = start + 2; pos + 1 < max; ++pos) {
            const c = state.src.charCodeAt(pos);
            if (c === newlineChar) {
                // no new line
                return false;
            } else if (c === closeBracketChar) {
                // no empty link "[[]]" allowed
                if (pos === start + 2) return false;
                // must end with a second ']'
                if (state.src.charCodeAt(pos + 1) !== closeBracketChar)
                    return false;
                foundEnd = true;
                break;
            }
        }

        if (!foundEnd) return false;

        // "[[content]]"
        //  ^        ^
        //  start    pos

        state.pos = start + 2;
        state.posMax = pos;

        const token = state.push("internal_link", "", 0);
        token.content = state.src.slice(start + 2, pos);

        state.pos = pos + 2;
        state.posMax = max;
        return true;
    }

    function render(
        tokens: Token[],
        idx: number,
        mdopts: MarkdownIt.Options,
        env: unknown,
        self: Renderer
    ) {
        const token = tokens[idx];
        const content = token.content;

        const result =
            typeof options === "function" ? options(content, env) : options;

        if (typeof result === "string") {
            return result;
        }

        const attrs: OptionsAsAttrs = Object.assign(
            { href: content, text: content },
            result
        );

        Object.keys(attrs).forEach((key) => {
            const attr = attrs[key];
            if (typeof attr === "function") {
                attrs[key] = attr(content, env);
            }
        });

        const text = attrs.text;
        delete attrs.text;

        token.attrs = Object.keys(attrs)
            .sort()
            .map((key) => [key, attrs[key] as string]);

        return `<a${self.renderAttrs(token)}>${text}</a>`;
    }

    md.inline.ruler.push("internal_link", tokenize);
    md.renderer.rules.internal_link = render;
}
