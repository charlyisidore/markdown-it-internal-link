/* eslint @typescript-eslint/no-var-requires: "off" */
const markdownIt = require("markdown-it");
const markdownItInternalLink = require("./");

describe("No option", () => {
    const md = markdownIt().use(markdownItInternalLink);

    test("Simple link", () => {
        const source = `[[foo]]`;
        const target = `<p><a href="foo">foo</a></p>\n`;
        expect(md.render(source)).toEqual(target);
    });

    test("Link surrounded by text", () => {
        const source = `foo[[bar]]baz`;
        const target = `<p>foo<a href="bar">bar</a>baz</p>\n`;
        expect(md.render(source)).toEqual(target);
    });

    test("Mix links and markdown syntax", () => {
        const source = `**[[foo]]** [[**bar**]]`;
        const target = `<p><strong><a href="foo">foo</a></strong> <a href="**bar**">**bar**</a></p>\n`;
        expect(md.render(source)).toEqual(target);
    });

    test("Non terminated link", () => {
        const source = `[[foo]`;
        const target = `<p>[[foo]</p>\n`;
        expect(md.render(source)).toEqual(target);
    });

    test("Missing second closing bracket", () => {
        const source = `[[foo]bar`;
        const target = `<p>[[foo]bar</p>\n`;
        expect(md.render(source)).toEqual(target);
    });

    test("Space between closing brackets", () => {
        const source = `[[foo] ]`;
        const target = `<p>[[foo] ]</p>\n`;
        expect(md.render(source)).toEqual(target);
    });

    test("Line break", () => {
        const source = `[[foo\nbar]]`;
        const target = `<p>[[foo\nbar]]</p>\n`;
        expect(md.render(source)).toEqual(target);
    });

    test("Empty link at the end", () => {
        const source = `foo[[]]`;
        const target = `<p>foo[[]]</p>\n`;
        expect(md.render(source)).toEqual(target);
    });

    test("Empty link surrounded by text", () => {
        const source = `foo[[]]bar`;
        const target = `<p>foo[[]]bar</p>\n`;
        expect(md.render(source)).toEqual(target);
    });

    test("No link but still executes the rule", () => {
        const source = `!foobar`;
        const target = `<p>!foobar</p>\n`;
        expect(md.render(source)).toEqual(target);
    });
});

describe("Options as object", () => {
    const md = markdownIt().use(markdownItInternalLink, {
        class: "wikilink",
        href: (content) => {
            const pos = content.indexOf("|");
            return pos === -1 ? content : content.substring(0, pos);
        },
        text: (content) => {
            const pos = content.indexOf("|");
            return pos === -1 ? content : content.substring(pos + 1);
        },
    });

    test("Wikilink without label", () => {
        const source = `[[foo]]`;
        const target = `<p><a class="wikilink" href="foo">foo</a></p>\n`;
        expect(md.render(source)).toEqual(target);
    });

    test("Wikilink with label", () => {
        const source = `[[foo|bar]]`;
        const target = `<p><a class="wikilink" href="foo">bar</a></p>\n`;
        expect(md.render(source)).toEqual(target);
    });
});

describe("Options as function returning object", () => {
    const md = markdownIt().use(markdownItInternalLink, (content) => {
        const pos = content.indexOf("|");
        return {
            class: "wikilink",
            href: pos === -1 ? content : content.substring(0, pos),
            text: pos === -1 ? content : content.substring(pos + 1),
        };
    });

    test("Wikilink without label", () => {
        const source = `[[foo]]`;
        const target = `<p><a class="wikilink" href="foo">foo</a></p>\n`;
        expect(md.render(source)).toEqual(target);
    });

    test("Wikilink with label", () => {
        const source = `[[foo|bar]]`;
        const target = `<p><a class="wikilink" href="foo">bar</a></p>\n`;
        expect(md.render(source)).toEqual(target);
    });
});

describe("Options as function returning string", () => {
    const md = markdownIt().use(
        markdownItInternalLink,
        (content) => `<button>${content}</button>`
    );

    test("Render button", () => {
        const source = `[[foo]]`;
        const target = `<p><button>foo</button></p>\n`;
        expect(md.render(source)).toEqual(target);
    });
});

describe("Using env", () => {
    const md = markdownIt().use(markdownItInternalLink, (content, env) =>
        env.internalLinks.push(content)
    );

    test("Store links", () => {
        const source = `[[foo]] and [[bar]]`;
        const target = `<p><a href="foo">foo</a> and <a href="bar">bar</a></p>\n`;
        const sourceEnv = { internalLinks: [] };
        const targetEnv = { internalLinks: ["foo", "bar"] };
        expect(md.render(source, sourceEnv)).toEqual(target);
        expect(sourceEnv).toEqual(targetEnv);
    });
});
